﻿/// <reference path="~/GeneratedArtifacts/viewModel.js" />

myapp.ViewPanel.Details_postRender = function (element, contentItem) {
    // Write code here.
    var name = contentItem.screen.Panel.details.getModel()[':@SummaryProperty'].property.name;
    contentItem.dataBind("screen.Panel." + name, function (value) {
        contentItem.screen.details.displayName = value;
    });
}

