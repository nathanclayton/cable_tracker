﻿/// <reference path="~/GeneratedArtifacts/viewModel.js" />

myapp.ViewCircuit.Details_postRender = function (element, contentItem) {
    // Write code here.
    var name = contentItem.screen.Circuit.details.getModel()[':@SummaryProperty'].property.name;
    contentItem.dataBind("screen.Circuit." + name, function (value) {
        contentItem.screen.details.displayName = value;
    });
}

